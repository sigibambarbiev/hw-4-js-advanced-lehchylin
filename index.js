const starWars = document.querySelector('#sw');

function sortFilms(url) {

    fetch(url)
        .then(response => response.json())
        .then(filmInfo => {

            filmInfo.forEach(elem => {

                const epis = document.createElement('p');
                starWars.append((epis));
                epis.classList.add('epis');
                epis.innerText = `episod namber: ${elem.episodeId}`;

                const name = document.createElement('p');
                starWars.append(name);
                name.classList.add('name');
                name.innerText = `episode name: "${elem.name.toUpperCase()}"`;

                const person = document.createElement('ul');
                starWars.append(person);
                person.classList.add('person');
                person.innerText = 'characters:';

                const openingCrawl = document.createElement('p');
                starWars.append(openingCrawl);
                openingCrawl.innerText = `descriptiins -- ${elem.openingCrawl}`;

                elem.characters.forEach(character => {

                    fetch(character)
                        .then(response => response.json())
                        .then(response => {

                            const hero = document.createElement('li');
                            person.append(hero);
                            hero.classList.add('hero');
                            hero.innerText = response.name;

                        });
                });
            });
        })
        .catch(error => {
            console.error('Виникла проблема з Вашим fetch-запросом: ' + error);
        });
}

sortFilms('https://ajax.test-danit.com/api/swapi/films');

